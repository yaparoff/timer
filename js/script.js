var pomodoroLink = document.querySelector('#pomodoro-link');
var shortTimeLink = document.querySelector('#short-time-link');
var longTimeLink = document.querySelector('#long-time-link');
var timerOptions = document.querySelector('.timer__options');
var timerDisplay = document.querySelector('.timer__display');
var signLink = document.querySelector('.sign-link');
var btnStart = document.querySelector('#btn-start');
var btnStop = document.querySelector('#btn-stop');
var btnReset = document.querySelector('#btn-reset');
var timerMinutes = document.querySelector('#timerMinutes');
var timerSeconds = document.querySelector('#timerSeconds');

var pomodoroTime = 25*60;
var shortTime = 5*60;
var longTime = 10*60;
var selectedItem;

function addZero(num) {
    if( num <= 9 ) {
        return '0' + num;
    } else {
        return num;
    }
}
function highlight(node) {
    if (selectedItem) {
        selectedItem.classList.remove('timer__option--active');
    }
    selectedItem = node;
    selectedItem.classList.add('timer__option--active');
    return window.selectedItem;
}

function convertToMinutes(sec) {
    var minutes = Math.floor(sec / 60);
    var seconds = sec % 60;
    var remainMinutes = addZero(minutes);
    var remainSeconds = addZero(seconds);
    
    return obj = {
        'minutes': remainMinutes,
        'seconds': remainSeconds
    }
}
function convertToSeconds(time) {
    var seconds = +timerMinutes.innerHTML * 60;
    var summarySeconds = seconds + (+timerSeconds.innerHTML);
    return summarySeconds;
}
function initializeTimer(param) {
    var seconds;
    if(param == 'pomodoroTime') {
        seconds = pomodoroTime;
    } else if(param == 'shortTime') {
        seconds = shortTime;
    } else if(param == 'longTime') {
        seconds = longTime;
    } else if(typeof param == 'number') {
        seconds = param;
    }
    else {
        seconds = pomodoroTime;
    }
    convertToMinutes(seconds);
    timerMinutes.innerHTML = obj.minutes;
    timerSeconds.innerHTML = obj.seconds;
}
function updateTimer() {
    var seconds = convertToSeconds();
    window.timer = setInterval(function() {
        seconds = seconds - 1;
        initializeTimer(seconds);        
    }, 1000);
}

function tick(sec) {
    sec = sec - 1;
    initializeTimer(sec);
}
initializeTimer('pomodoroTime');

function stop() {
    window.clearInterval(window.timer);
}

timerOptions.addEventListener('click', function(event) {
    var target = event.target;
    while (target != timerOptions) {
        if (target.tagName == 'DIV') {
            highlight(target);
            window.clearInterval(window.timer);
            if(target.id == 'pomodoro-link') {
                initializeTimer('pomodoroTime');
            } else if(target.id == 'short-time-link') {
                initializeTimer('shortTime');
            } else if(target.id == 'long-time-link') {
                initializeTimer('longTime');
            }
            return;
        }
        target = target.parentNode;
    }
});

btnStart.addEventListener('click', updateTimer);
btnStop.addEventListener('click', stop);